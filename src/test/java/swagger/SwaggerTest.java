package swagger;

import api.Specifications;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class SwaggerTest {

    private final static String URL = "https://petstore.swagger.io/v2";

    @Test
    public void findByStatusTest(){
        Specifications.installSpecification(Specifications.requestSpec(URL), Specifications.responseSpecOK200());

        Tag tag = new Tag(1, "#dog");
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(0,tag);

        Category category = new Category(1, "animal");

        String[] photoUrl = {"https: //site.com/bulldog/photo"};

        String status = "available";

        PostAddPet request = new PostAddPet(10, category, "Bulldog", photoUrl, tags,"available");
        PostAddPet response =
                given()
                .body(request)
                .when()
                .post("/pet")
                .then().log().all()
                .extract().as(PostAddPet.class);
        Assert.assertTrue(request.equals(response));
    }
}
