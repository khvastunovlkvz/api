package swagger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class PostAddPet {
    private Integer id;
    private Category category;
    private String name;
    private String[] photoUrls;
    private ArrayList<Tag> tags;
    private String status;

    public PostAddPet(Integer id, Category category, String name, String[] photoUrls, ArrayList<Tag> tags, String status) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.photoUrls = photoUrls;
        this.tags = tags;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String[] getPhotoUrls() {
        return photoUrls;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PostAddPet that = (PostAddPet) o;
        return  Objects.equals(id, that.id) &&
                category.equals(that.category) &&
                Objects.equals(name, that.name) &&
                Arrays.equals(photoUrls, that.photoUrls) &&
                tags.equals(that.tags) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, category, name, tags, status);
        result = 31 * result + Arrays.hashCode(photoUrls);
        return result;
    }
}
