package api;

public class UserTimeResponse{

    private String name;
    private String job;
    private String updatedAT;

    public UserTimeResponse(String name, String job, String updatedAT) {
        this.name = name;
        this.job = job;
        this.updatedAT = updatedAT;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public String getUpdatedAT() {
        return updatedAT;
    }
}
